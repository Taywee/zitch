.. _Arlen:

=====
Arlen
=====
The Darkling in Arlen perhaps has a greater goal in him.  It wants to find a way
back into the feywild (direct ways in and out aren't known to many races or
groups of people).  Maybe it is an exile, or perhaps just a lost fae who can not
venture back into the faewild and needs passage back.  This fae communicates,
and has mentioned of strange natural reactions in the faewild, a flourishing of
life and changing of form that may be of interest to an alchemist.  Arlen's deal
with the Darkling is based on something of his own motivation.  Perhaps
treasure, perhaps knowledge, perhaps even just the chance to help an innocent
creature get home.

Maybe Arlen resisted the possession at first, but eventually began to feel the
darkling's feelings and empathized.  He feels the darkling's fear, anxiety, and
homesickness for his own world.

Specific story: When Arlen was possessed, he initially didn't know what had
happened and was panicked.  The possession would possess his hands, and he would
write in strange scripts without Arlen's being able to control it, all over his
own arms.  He was to beg help from the masters when Zitch came upon him, and
recognized the scrawlings on his arms as belonging to a magical script.  With a
flick of his finger, he activated his Goggles of Comprehend Written Languages
and deciphered that it was Sylvan, and was panicked begs for help.  Rather than
reporting to the master, Arlen and Zitch worked together to communicate with the
being, and doing specific research into channeling creatures (DM, we should
probably have developed a particular proficiency for communicating with fae
creatures [maybe a bonus on charisma rolls targeting fae], or maybe knowledge of
sylvan.  Arlen definitely knows sylvan from this, having a fae in his head)
until Arlen could willingly channel the Darkling, and communicate with him
directly in his head.  He could even willingly and temporarily give the Darkling
control over limbs or even his entirely body.  The darkling, as a gift,
described through Arlen with enough detail to Zitch a formula for a magical fae
item.  Zitch just finishes it when they arrive at the beginning of the campaign.

Taktak helped for some of this work.  He was aware of the secret, and they were
the only three who were, but he was also on the verge of completing what he
considered "an alchemic masterwork" that would elevate him to the level of a
master and enable him to petition for the means to take expeditions to advance
his knowledge and develop his discoveries very quickly.

Something had gone wrong with the alcemy , and he died in an explosion.  Arlen
had gotten to taktak first and attempted to administer first aid in the form of
a potion in the room and also attempted to make a healing salve as he remembered
from his tutorship on herbs, using Taktak's herbalism kit that was nearby.  It
was no good, [in mechanics justification, the explosion did more than double
Taktak's HP in damage, so it was an instant kill].  Zitch petitioned the masters
to revive Taktak, but the masters would not, because necromancy was expressly
forbidden in the university (necromancy school spells were done, but limited,
due to religious opposition to the revival of the dead, the shared god of all
the masters forbids it)

Arlen's primary motive for going over the great divide, perhaps, is one of
mercy.  He thinks that maybe there is a way into the faewild on the other side
of the ocean.  He also wants to accompany his friend who helped save his friend,
and is pleased to be able to continue traveling side-by-side, especially after
the loss of Taktak.  Arlen wasn't as close with Taktak as Zitch was, but he was
still stunned and shaken by his loss.  They didn't have the bond of the drive to
restore their species in common, but Taktak and Arlen still developed what could
be called a close friendship.

Maybe ask Brandon and Arlen (could just do alone) for the possibility of an
early role-playing session (maybe 30 minutes) to detail the leave from the
academy and travel to the southwest with Arlen.  Even just briefly.
