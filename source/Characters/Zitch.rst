.. _Zitch:

================
Zitch
================

Zitch is a Kenku alchemist.

Backstory
---------

Time with Arlen
^^^^^^^^^^^^^^^
Around this time, Arlen became possessed by a Darkling. When Arlen was
possessed, he initially didn't know what had happened and was panicked.  The
possession would possess his hands, and he would write in strange scripts
without Arlen's being able to control it, all over his own arms.  He was to beg
help from the masters when Zitch came upon him, and recognized the scrawlings on
his arms as belonging to a magical script.  With a flick of his finger, he
activated his Goggles of Comprehend Written Languages and deciphered that it was
Sylvan, and was panicked begs for help.  Rather than reporting to the master,
Arlen and Zitch worked together to communicate with the being, and doing
specific research into channeling creatures (DM, we should probably have
developed a particular proficiency for communicating with fae creatures [maybe a
bonus on charisma rolls targeting fae], or maybe knowledge of sylvan.  Arlen
definitely knows sylvan from this, having a fae in his head) until Arlen could
willingly channel the Darkling, and communicate with him directly in his head.
He could even willingly and temporarily give the Darkling control over limbs or
even his entirely body.  The darkling, as a gift, described through Arlen with
enough detail to Zitch a formula for a magical fae item.  Zitch just finishes it
when they arrive at the beginning of the campaign.

Taktak helped for some of this work.  He was aware of the secret, and they were
the only three who were, but he was also on the verge of completing what he
considered "an alchemic masterwork" that would elevate him to the level of a
master and enable him to petition for the means to take expeditions to advance
his knowledge and develop his discoveries very quickly.

Something had gone wrong with the alchemy , and he died in an explosion.  Arlen
had gotten to taktak first and attempted to administer first aid in the form of
a potion in the room and also attempted to make a healing salve as he remembered
from his tutorship on herbs, using Taktak's herbalism kit that was nearby.  It
was no good.  Zitch petitioned the masters
to revive Taktak, but the masters would not, because necromancy was expressly
forbidden in the university (necromancy school spells were done, but limited,
due to religious opposition to the revival of the dead, the shared god of all
the masters forbids it).

Arlen's primary motive for going over the great divide, perhaps, is one of
mercy.  He thinks that maybe there is a way into the faewild on the other side
of the ocean.  He also wants to accompany his friend who helped save his friend,
and is pleased to be able to continue traveling side-by-side, especially after
the loss of Taktak.  Arlen wasn't as close with Taktak as Zitch was, but he was
still stunned and shaken by his loss.  They didn't have the bond of the drive to
restore their species in common, but Taktak and Arlen still developed what could
be called a close friendship.

Journal
-------

2019-02-26
^^^^^^^^^^

* Boat top layer shiphand.
* Arani Mor, region across the sea.
* Ship is The Mist.
* Lollani has a bow with a magical aura.
* Magic at the top of the tower (Conjuration).
* The tower had a map and some magical embers.  We decided to move west toward the closest town we could identify on the map.
* Aloreus curse.
* Aldeous is lord in East Aroni Mor.
* King Ricart, generations ago, struck a pact with a fiend.  Land cursed, eternal night.
* Aldeous was one of the lords of King Ricart.  Aldus leads an army of undead.
* Lorian had an apprentice, Morelei, who took over Lorean's Rest.
* Gaultier Waycrest leader of Mistshire.
* Morelei:  Mad.  Long black hair.  Average height 5'8".  Lost hair, wore black clothing.  Isolated in tower.  Patchy hair.  25 years old.
* Statue is not carved directly into the stone.  Just placed in front.

2019-03-05
^^^^^^^^^^

* Protection from Evil and Good carved on bords in front of the door of tower.
* Magic-looking book in the tower of :ref:`Lorian` in :ref:`Mistshire`.  Ended up being blank.
* Source of magic in tower behind wall.
* "the Fey takes some time to read the runes, occasionally taking and mumbling to himself in your head.
* after 3 hours of studying, he tells you that this cipher is most likely a code related to ancient necromancy and abyssal fiends
* used by ancient necromancers and worshipers of specific fiends but is unable to tell which"

2019-03-12
^^^^^^^^^^

* Fire has conjuration magic.
* Moralei became an allip.
* Lorian in Lorian's Rest.
* Lorian wants us to bury Moralei.
* Spell is Teleportation Circle.
* Arlen pissed off some undead things.

2019-03-19
^^^^^^^^^^

* Magical spur: magical item Spectral Tack.  Requires attunement.  Wonderous Legendary item.
* You can use an action to summon a Nightmare.  The nightmare appears at the start of your turn within 20 feet of you.  It attacks as your ally, with your intiative count.  etc etc (DM will give you the details).
* The darkling would have saved Arlen's life if necessary.

2019-05-07
^^^^^^^^^^

* Flew the Roc northwest toward :ref:`Trehearn`.
* Landed in the early afternoon.
* Statue in town of :ref:`Pelor`.

2019-05-14
^^^^^^^^^^

* Arlen almost died in the forest.  The Darkling brought him back to life.

2019-08-10
^^^^^^^^^^

* Rael said "I am the martyr.  I am here to take you back. (or 'Return from
  where you came')"

2019-08-24
^^^^^^^^^^

* Met Erdan. Leader of :ref:`Tiri-Ithil`.
* Drow priestess taken captive by underdark drow.  Underdark drow live under
  these drow.
* Clara is the priestess who disappeared.
* Tia is the replacement priestess
