.. _Aldeous:

================
Aldeous
================
Created Tuesday 05 March 2019

Was a lord of :ref:`King Ricart`.  Leads an army of the undead.
