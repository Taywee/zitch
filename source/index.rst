.. Zitch documentation master file, created by
   sphinx-quickstart on Sat Aug 10 18:24:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Zitch's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   /Places
   /Monsters
   /Characters
   /Political

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
